<?php

/**
 * Attempt to dump and zip all the requirements for a site/site instance.
 * 
 */

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * @return   An associative array describing your command(s).
 */
function bundle_drush_command() {
  $items = array();

  $items['bundle'] = array(
    'description' => "Export a bundle containing everything in this drupal instance",
    'callback' => 'bundle_drush_bundle',
    'options' => array(
      '--nozip' => 'do not zip up the results when done',
      '--nofile' => 'exclude the sites files directory',
      '--path' => 'create a tarball in the named location',
      '--package-handler' => 'specify to use svn or something'
     ),
    'arguments' => array(
    ),
    'examples' => array(
      'bundle --path=/tmp/mysite' => 'create a tarball in the named location',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

function bundle_drush_bundle() {

  // Copy the files to the build target location
  $command = drush_get_command();
  $arguments = $command['arguments'];

  $thisname = basename(drush_get_context('DRUSH_DRUPAL_SITE_ROOT'));
  $local_settings = drush_sitealias_get_record('@' . $thisname);
  if (empty($local_settings)) {
    drush_sitealias_get_record($thisname) ;
  }
  if (empty($local_settings)) {
    drush_log("No site settings for this site ($thisname) found", 'error');
    return FALSE;
  }
  
  
}