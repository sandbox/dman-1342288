<?php

require_once('simpletest/web_tester.php');


/**
 * Engine that can log in and do things to a remote site
 */
class drush_remote_runner extends WebTestCase {
  /**
   * Expected parameter is a site-alias settings array.
   * It should include url, drupal-name and drupal-pass
   */
  function drush_remote_runner($label, $settings) {
    $this->settings = $settings;
    drush_log("Initialized drush_remote_runner", 'success');
  }

  function url($path) {
    return 'http://' . $this->settings['uri'] . $path; 
  }
  
  function run_method($method) {
    if (! method_exists($this, $method)) {
      drush_log("Couldn't run $method on the remote runner, Method doesn't exist.", 'warning');
      return FALSE;
    }
    drush_log("Running $method", 'info');
    $this->_reporter = new TextReporter(); #&$reporter;
    $invoker = &$this->_reporter->createInvoker($this->createInvoker());
    $invoker->before($method);
    $invoker->invoke($method);
    $invoker->after($method);

    $this->_reporter->paintCaseEnd($this->getLabel());
    unset($this->_reporter);
  }
  

  function login() {
    $url = $this->url('/user');
    if (empty($this->settings['drupal-user']) || empty($this->settings['drupal-pass'])) {
      drush_log("To log in to the remote site, we need drupal-user and drupal-pass set in the site-alias info", 'warning');
      drush_log("Cannot log in to $url", 'error');
      return FALSE;
    }

    #$form_id = 'user-login';  
    drush_log(dt('Attempting to log in to %uri as %drupal-user', array('%uri' => $url, '%drupal-user' => $this->settings['drupal-user'], )), 'info');

    $this->get($url);
    #$this->showHeaders();
    $this->setField('name', $this->settings['drupal-user']);
    $this->setField('pass', $this->settings['drupal-pass']);
    $this->click("Log in");
    // Page loads. If there is a log out button, we must be in!
    $this->assertLink('Log out');
    $this->assertPattern('!href="/logout"!');
    #drush_log($this->showSource(), 'debug');
    drush_log($this->_browser->getContent(), 'debug');
    drush_log('Logged in', 'success');
    return TRUE;
  }


  function enable_php() {
    $this->login();
    drush_log('Loading modules page', 'info');
    $path = '/admin/build/modules';
    $url = $this->url($path);
    $this->get($url);
    drush_log($this->_browser->getContent(), 'debug');
    
    $this->setField('status[php]', 'php');
    $this->click("Save configuration");
    drush_log($this->_browser->getContent(), 'debug');
    // Should check that went ok.
  }

}

