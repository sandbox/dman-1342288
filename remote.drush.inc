<?php

/**
 * Brutally do some things on a remote site you don't have SSH access to
 * 
 * 
 */

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * @return   An associative array describing your command(s).
 */
function remote_drush_command() {
  $items = array();

  $items['remote-init'] = array(
    'description' => "Initializes a backdoor into the remote site to allow us to start sending commands",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );


  $items['remote'] = array(
    'description' => "Do things on the remote site via the web interface",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
  );

  return $items;
}


function drush_remote_init() {
print_r("running  " . __FUNCTION__);
  $settings = drush_synch_get_site_settings();
print_r(__FUNCTION__ . " got current site settings   ");
print_r(__FUNCTION__ . " Reading opts   ");

  // Read param overrides if not set in peer-settings.
  foreach(array('uri', 'drupal-user', 'drupal-pass') as $param) {
    if (drush_get_option(array($param), NULL)) {
      $peer_settings[$param] = drush_get_option(array($param), NULL);
    }
    if (empty($peer_settings[$param])) {
      $peer_settings[$param] = drush_get_option(array($param), NULL);
    }
    else {
      drush_log("Don't yet know what the peer credentials for $param are", 'warning');
    }
  }
  
  if (isset($peer_settings['uri'])){
    $peer_settings = drush_synch_get_peer_settings($settings);
  }
  else {
    drush_log('Don\'t yet know what the peer credentials are', 'warning');
  }

  // If not run from an existing site context, the remote preferences must be civen as args
  drush_bootstrap_max();

print_r(get_defined_vars());
  print_r($peer_settings);

  if (!$peer_settings['uri'] || !$peer_settings['drupal-user'] || !$peer_settings['drupal-pass']) {
    drush_log('Need a drupal uri, username and password defined or a working peer context to init a remote download from scratch.', 'error');
    drush_log('Call this function with -uri=example.com -drupal-user=phpuser -drupal-pass=web', 'warning');
    return FALSE;
  }
  
  drush_log('Ready to log in to ', 'info');
  
  
  if (! drush_remote_login($peer_settings) ) {
    drush_log(dt('Log in failed', array()), 'error');
    return FALSE;
  }
  
  drush_remote_enable_php($peer_settings);
  
  #drush_remote_create_php_block($peer_settings);
}



function drush_remote_login($remote_settings) {
  if (empty($remote_settings['drupal-user']) || empty($remote_settings['drupal-pass'])) {
    drush_log("Need drupal-user and drupal-pass set in the site-alias info", 'warning');
    return FALSE;
  }

  $login_url = 'http://' . $remote_settings['uri'] . '/user';
  $form_id = 'user-login';
  
  drush_log(dt('Attempting to log in to %uri as %drupal-user', array('%uri' => $login_url, '%drupal-user' => $remote_settings['drupal-user'], )), 'info');

  $response = drush_remote_post_form($login_url);
  $field_values = drush_remote_fetch_form_fields($response, $form_id);
  drush_log(dt('Got a copy of the login form. Now submitting it', array()), 'info');

  $post_data = $field_values;
  $post_data['name'] = $remote_settings['drupal-user'];
  $post_data['pass'] = $remote_settings['drupal-pass'];

  $response = drush_remote_post_form($login_url, $post_data);

#print_r($response);  

  return TRUE;
}





/**
 * We will inject the code into the php-eval area of the block visibility
 * 
 * Make a block for this
 */
function drush_remote_create_php_block($remote_settings) {
  $url = 'http://' . $remote_settings['uri'] . '/admin/build/block/add'; 
  $form_id = 'block-add-block-form'; 
  $block_description = 'drush-remote temp block';
  $response = drush_remote_post_form($url);
  $field_values = drush_remote_fetch_form_fields($response, $form_id);
  $field_values = array_merge($field_values, array(
    'info' => $block_description,
    'title' => $block_description,
    'body' => 'delete me',
    'visibility' => 2, # trigger PHP execution
    'pages' => '<?'.'php drupal_set_message("Hello World") ?'.'>', # PHP execution
  ));
  
print_r($field_values);
  $response = drush_remote_post_form($url, $field_values);
  // This should return the page saying 'block is created'


  // It's tricky to know what the new ID id though
  $url = 'http://' . $remote_settings['uri'] . '/admin/build/block'; 
  $response = drush_remote_post_form($url);
  
  $dom = new DOMDocument();
  $dom->loadHTML($response);
  $xpath = new DOMXPath($dom);
  #$query = "//table[@id = 'blocks']//tr[contains( . , '$block_description')]";
  $query = "//table[@id = 'blocks']//tr[contains( . , '$block_description')]//select[contains(@class, 'block-region-select')]";
  // hopefully found the selector to position the block in a region
  $found_list = $xpath->query($query);
  foreach ($found_list as $block_position) {
    $new_values = array($block_position->getAttribute('name') => 'content');
  }
  
  // Enable it so the code gets called.
  // Retrieve the form again
  $form_id = 'block-admin-display-form'; 
  $field_values = drush_remote_fetch_form_fields($response, $form_id);
  $field_values = array_merge($field_values, $new_values);
  $response = drush_remote_post_form($url, $field_values);    
}


function drush_remote_post_form($url, $post_data = array()) {
  foreach ($post_data as $key => $val) {
    if (is_null($val)) unset ($post_data[$key]);
  }
  
  print "\nPosting to $url\n";
  if (! empty($post_data) ) print_r($post_data);
  
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);        
  curl_setopt($ch, CURLOPT_HEADER, 0); 

  curl_setopt($ch, CURLOPT_COOKIESESSION, TRUE); 
  curl_setopt($ch, CURLOPT_COOKIEFILE, "remote_drush-cookiefile.txt"); 
  curl_setopt($ch, CURLOPT_COOKIEJAR, "remote_drush-cookiefile.txt"); 

  curl_setopt($ch, CURLOPT_POST, 1);  
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
  $response = curl_exec($ch);
  drush_print("Got a result ". strlen($response) ." long from $url");

  if (empty($response)) {
    drush_log("Empty response from Curl request", 'error');
    print_r(curl_getinfo($ch));
  } else {
    print(" : Message :\n > " . join("\n > ", drush_remote_fetch_messages_from_page($response)) . "\n");
  }

  return $response;
}

/**
 * Requests an URL, and returns the names of the fields that that form expects
 * us to fill in, along with the current values.
 * 
 * Required if retrieving form tokens
 */
function drush_remote_fetch_form_fields($source, $form_id) {
  if (empty($source)) {
    return array();
  }

  // Now parse the DOM of that HTML response to find the required form fields
  $dom = new DOMDocument();
  $dom->loadHTML($source);

  #drush_print("Parsed the HTML");

  $xpath = new DOMXPath($dom);
  $xpath->registerNamespace('xhtml', "http://www.w3.org/1999/xhtml");
  
  // Find fields
  $query = "//form[@id='$form_id']//input|//form[@id='$form_id']//textarea|//form[@id='$form_id']//select";
  $field_list = $xpath->query($query);
  drush_print("found ". $field_list->length ." data fields in form $form_id");
  $field_values = array();
  foreach ($field_list as $field_element) {
    #print "\n". $dom->saveXML( $field_element );
    $name = $field_element->getAttribute('name');
    $value = $field_element->getAttribute('value');
    $type = $field_element->getAttribute('type');
    if ($field_element->nodeName == 'select') {
      $type = $field_element->nodeName;
    }
    
    
    // Break down the nested arrays - only one level.
    if (FALSE && preg_match('/(.*)\[(.*)\]/', $name, $matches)) {
      print_r($matches);
      $name = $matches[1];
      $key = $matches[2];
      $target = &$field_values[$name][$key];
    }
    else {
      $target = &$field_values[$name];
    }

    switch ($type) {
      case 'radio' :
        if ($field_element->getAttribute('checked')) {
          $target = $value;
        }
        break;
      case 'checkbox' :
        if ($field_element->getAttribute('checked')) {
          $target = $value;
        }
        else {
          $target = NULL;
        }
        break;
      case 'select' :
        foreach ($field_element->childNodes as $option_element) {
          if ($option_element->getAttribute('selected')) {
            $target = $option_element->getAttribute('value');
          }
        }
        break;
      case 'hidden' :
      case 'submit' :
      case 'text' :
      default :
        $target = $value;
    }
  }
  return $field_values;  
}

/**
 * Scan the page for the drupal message block
 */
function drush_remote_fetch_messages_from_page($html) {
  $dom = new DOMDocument();
  $dom->loadHTML($html);
  $xpath = new DOMXPath($dom);
  $xpath->registerNamespace('xhtml', "http://www.w3.org/1999/xhtml");
  
  // Find fields
  $query = "//*[contains(@class, 'messages')]";
  $field_list = $xpath->query($query);
  $field_values = array();
  foreach ($field_list as $field_element) {
    $field_values[] = trim($field_element->textContent);
  }
  return $field_values;
}






/////////////////

function drush_remote() {
  $args = func_get_args();
  if (empty($args)) {
    drush_log("Need a command to run remotely.", 'warning');
    return FALSE;
  }
  
  $settings = drush_synch_get_site_settings();
  $peer_settings = drush_synch_get_peer_settings($settings);

  drush_log("Peer to log in to will be " . $peer_settings['uri'], 'success');
  require_once(dirname(__FILE__) . '/drush_remote_runner.class.inc');
  $runner = new drush_remote_runner($peer_settings['uri'], $peer_settings);

  call_user_method_array('run_method', $runner, $args);
  
  #$runner->run_method($args);
}

 
