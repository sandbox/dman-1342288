<?php

/**
 * Interactive setup to assist first setup when cloning a remote site.
 * 
 * Requires drush-cli for commandline interaction.
 * 
 * 
 */
define('INFO_INDENT', 4);
/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * @return   An associative array describing your command(s).
 */
function clone_drush_command() {
  extract($GLOBALS['text_decorations']);
  $items = array();

  $items['clone'] = array(
    'description' => "Starts an interactive session to guide you through copying a site from a remote installation",
    'callback' => 'drush_clone_wizard',
    'options' => array(
      'host' => "The remote target. Do not use remote-host as drush will interpret that itself.",
      'user' => "The username on the remote target. Do not use remote-user as drush will interpret that itself.",
     ),
    'arguments' => array(
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['site-alias-setup'] = array(
    'description' => "Starts an interactive session to guide you through setting up an alias and connection details for a remote site",
    'callback' => 'drush_clone_alias_wizard',
    'options' => array(
      'host' => "The remote target. Do not use remote-host as drush will interpret that itself.",
      'user' => "The username on the remote target. Do not use remote-user as drush will interpret that itself.",
     ),
    'arguments' => array(
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Create and write a site alias file 
 * 
 * The site name can be given either as a straight command argument or as a --
 * name parameter. Other parameters are also allowed to be passed in on the
 * commandline.
 */
function drush_clone_alias_wizard() {
  extract($GLOBALS['text_decorations']);
  $command = drush_get_command();
  $arguments = $command['arguments'];

  // Initialize the cli utilities, including readline support
  drush_cli_pre_cli();
  define('DRUSH_READLINE_PREFILLED', 'clone_readline_prefilled');
  
  drush_print("{$START}Preparing to set up an alias entry{$_START}");
  drush_print("This will assist in the creation of a drush site_alias record that can be used for subsequent synchronizations.", INFO_INDENT);
  drush_print("If this is your first time, try running this process with -v (verbose) on to see more notes about what the choices mean.", INFO_INDENT);
  
  $thisname = basename(drush_get_context('DRUSH_DRUPAL_SITE_ROOT'));

  // @self is actually useless, it doesn't load any existing match alias settings.
  // so load self by name
  $local_settings = drush_sitealias_get_record('@' . $thisname);
  if (empty($local_settings)) {
    drush_sitealias_get_record($thisname) ;
  }
  if (empty($local_settings)) {
    drush_log("No pre-existing settings found anywhere in the current environment matching $thisname - making a new site_settings up", 'info');
    $local_settings = drush_sitealias_get_record('@self');
  }
  
  // As we will save the settings lite, avoid loading the transient defaults if we can
  // _drush_sitealias_add_transient_defaults($local_settings);
  
  $local_settings['name'] = drush_sitealias_uri_to_site_dir($local_settings['uri']);
  $expected_alias_filepath = clone_alias_file_path($local_settings, FALSE);

  if (! file_exists($expected_alias_filepath)) {
    drush_log("Did NOT find an existing local alias definition in the expected place. If we save this process, a new file will be created at $expected_alias_filepath This is normal for a first-time run.", 'warning');
    clone_save_alias($local_settings, $expected_alias_filepath);
  }
  drush_log("Local site is {$HIGHLIGHT}{$local_settings['name']}{$_HIGHLIGHT}", 'info');
  
  // Do we want to modify it anyway?
  

  drush_log("So we know what we know about the local site, now seeing what we need to know about a remote peer.", 'info');

  // Figure out the peer. It may be named in the commandline, or
  // in the settings['peer'] already.
  // If neither, ask for a new name.
  if (!empty($arguments)) {
    // Given a site alias already. preload what we already know
    $remote_alias = reset($arguments);
  }
  else if (! empty($local_settings['peer'])){
    $remote_alias = $local_settings['peer'];
  }
  if (empty($local_settings['peer'])) {
    drush_print("Your local site settings do not yet define a known peer. Enter the name of an existing site-alias that we will be peering with, or a new name to start setting one up.");
    $remote_alias = drush_clone_choose_alias("(identifier for this sites remote peer)");
  }

  // Try to load it
  $connection_success = FALSE;
  $previous_settings = drush_sitealias_get_record($remote_alias);
  // Try by @alias if that didn't work
  if (empty($previous_settings)) {
    $previous_settings = drush_sitealias_get_record('@'. $remote_alias);
    // That worked, remember it
    if (!empty($previous_settings)) {
      $remote_alias = '@'. $remote_alias;
    }
  }

  if (!empty($previous_settings)) {
    drush_log("Found valid settings for $remote_alias, loading them for modificaton", 'info');
    print_r($previous_settings);
    $strings = array(
      '@host' => $previous_settings['remote-host'], 
      '@uri' => $previous_settings['uri']
    );
    drush_print(dt("{$START}Checking status of remote peer{$_START} @uri on @host" , $strings));
    // TODO test immediately here?
    $results = drush_do_site_command($previous_settings, 'status');
    #print_r($results);
    if ($results['error_status']) {
      drush_log(join("\n", $results['error_log']), 'error');
    }
  }
  else {
    drush_log("Don't yet know any valid settings for $remote_alias, We'll try starting with a default set of settings. YOU WILL WANT TO CHANGE THE DEFAULTS SHOWN HERE.", 'warning');
    // do NOT test a blank set
    $connection_success = FALSE;
    $previous_settings['name'] = $remote_alias;
  }
  
  // Allow a save of the local link to the remote peer name, before even trying it out
  if (empty($local_settings['peer'])) {
    if (clone_confirm_prefilled(dt("Do you want to save this peer name in the local site configs now (We've not tested it yet)?", array()), 'y')) {
      $local_settings['peer'] = $remote_alias;
      clone_save_alias($local_settings, $expected_alias_filepath);
    }
  }

  // Merge any known settings over top of the defaults.
  $remote_settings = array_merge(clone_default_settings(), $previous_settings);

  if ($connection_success) {
    drush_log(dt("Your {$INFO}connection{$_INFO} to the remote site for @uri on {$HIGHLIGHT}@host{$_HIGHLIGHT} seems good and works", $strings), 'success');
    $redo = clone_confirm_prefilled(dt("Do you want to change the remote settings anyway?", array()), 'n');
    $connection_success = ! $redo;
  }
  while (!$connection_success) {
    # interactively ask for connection details.
    $connection_success = clone_setup_connection_settings($remote_settings);
    if (! $connection_success) {
      drush_set_error('Connection failed', dt("Connection failed."));
      if (! drush_confirm("Try Again?")) {
        return FALSE;
      }
    }
  }

  // SSH etc connection is OK, now check Drupal-specific settings
  // Continue, also need to confirm the Drupal root and site dir.
  drush_print("{$START}Now checking the Drupal install status on the remote site{$_START}");

  $drupal_core_ok = clone_check_remote_drupal_core($remote_settings);;
  if ($drupal_core_ok) {
    drush_log(dt("Your Drupal installation and site folder on the remote host seem good", array()), 'ok');
    $redo = clone_confirm_prefilled(dt("Do you want to change the remote Drupal settings anyway?", array()), 'n');
    $drupal_core_ok = ! $redo;
  }
  while (!$drupal_core_ok) {
    # interactively ask for connection details.
    $drupal_core_ok = clone_setup_remote_drupal_settings($remote_settings);
    if (! $drupal_core_ok) {
      drush_set_error('Inspection of Drupal install failed', dt("Remote Drupal not found."));
      if (! drush_confirm("Try Again?")) {
        return FALSE;
      }
    }
  }
  $strings = array(
    '@root' => $remote_settings['root'],
    '@name' => $remote_settings['name']
  );
  drush_log(dt("It looks like there is something shaped like a Drupal site at @root", $strings), 'ok');

  // Success means we should save that peer name in the local settings
  $local_settings['peer'] = $remote_alias;
  clone_save_alias($local_settings, $expected_alias_filepath);

  drush_print(dt("{$HIGHLIGHT}Done{$_HIGHLIGHT} We now know enough about @name to start using it as a peer.", $strings));    
  drush_print(dt("You can run {$ACTION}drush site-alias @@name{$_ACTION} to review this info at any time.", $strings), INFO_INDENT);    
  drush_print(dt("You can run drush commands on that site by running drush with the alias name as the first argument,  eg"), INFO_INDENT);
  drush_print(dt("{$ACTION}drush @@name status{$_ACTION}.", $strings), INFO_INDENT);    
  drush_print(dt("{$ACTION}drush @@name site-alias --with-db @name{$_ACTION}.", $strings), INFO_INDENT);    

  return TRUE;
}

function drush_clone_wizard() {
  extract($GLOBALS['text_decorations']);
  $command = drush_get_command();
  $arguments = $command['arguments'];

  // Initialize the cli utilities, including readline support
  drush_cli_pre_cli();
  define('DRUSH_READLINE_PREFILLED', 'clone_readline_prefilled');
  
  drush_print("{$START}Preparing to clone a site{$_START}");
  drush_print("This will assist in connecting a local mirror to a remote site.", INFO_INDENT);

  // Ask for a known remote alias
  $prompt = "Enter the name of the remote site to connect to, eg [example.remotehost]. This should load an alias file (if already known) or prompt you to create one now.";
  $settings['peer'] = call_user_func(DRUSH_READLINE_PREFILLED, $prompt, $settings['peer']);

  // Immediately check if there is an alias file already that matches this name
  $peer_settings = drush_sitealias_get_record($settings['peer']);
  // Try by @alias if that didn't work
  if (empty($peer_settings)) {
    $peer_settings = drush_sitealias_get_record('@'. $settings['peer']);
    if ($peer_settings) {
      $settings['peer'] = '@'. $settings['peer'];
    }
  }

  if (!empty($peer_settings)) {
    drush_print(dt("Found settings for %peer, loading them for modificaton", array('%peer' => $settings['peer'])), INFO_INDENT);
    print_r($peer_settings);
  }
  else {
    drush_print("That site is unknown to me at the moment. Initiating setup of a peer alias");
    // Call this separate drush command and then come back.
    drush_clone_alias_wizard($settings['peer']);
    $peer_settings = drush_sitealias_get_record($settings['peer']);
  }

  // Verify remote connection
  if (! clone_check_remote_connection($peer_settings)) {
    drush_log("The remote connection defined in the peer settings was inaccessible or did not seem to contain a valid Drupal site. You'll have to fix this first. Try {$LOWLIGHT}drush site-alias-setup {$settings['peer']} {$_LOWLIGHT}", 'error');
    return FALSE;
  }
  if (! clone_check_remote_drupal_core($peer_settings)) {
    drush_log("The remote connection defined in the peer settings didn't seem to have a working drupal instance. This is OK if we are preparing a destination peer for upload. If you are attempting to download from there, this is a problem, and you should run {$LOWLIGHT}drush site-alias-setup {$settings['peer']} {$_LOWLIGHT}", 'error');
    return FALSE;
  }

  
  // Connection OK so far.
  drush_print("{$HIGHLIGHT}Clone Phase 2{$_HIGHLIGHT} Set up local instance destination.");    
  

  print_r($remote_settings);

  drush_print("{$HIGHLIGHT}Clone Phase 3{$_HIGHLIGHT} Remote site is good, start copying stuff down.");    
  
  print_r($peer_settings);
  
  return TRUE;
}

/**
 * Get user to run through the connection settings, possibly using commandline
 * arguments. Update them by reference.
 * 
 * This only adjusts SSH logins etc, not Drupal instance checking
 * 
 * Returns FALSE on failure of any sort.
 */
function clone_setup_connection_settings(&$settings) {
  extract($GLOBALS['text_decorations']);

  drush_log(dt("{$START}Setting up connection details.{$_START}"), 'info');

  $prompt = "Enter the name we will use to identify the site to connect to, eg [example.remotehost]. This will become the local alias and we will create an alias file of this name.";
  $settings['name'] = call_user_func(DRUSH_READLINE_PREFILLED, $prompt, $settings['name']);
  drush_print("If using ftp-only, enter the FTP username, server and pass here.", INFO_INDENT);

  $prompt = "Enter the (ssh) login address of the remote host, eg [example.isp.com]";
  $settings['remote-host'] = call_user_func(DRUSH_READLINE_PREFILLED, $prompt, $settings['remote-host']);
  $prompt = "Enter the username to log in as on the remote host, eg [webmaster]";
  $settings['remote-user'] = call_user_func(DRUSH_READLINE_PREFILLED, $prompt, $settings['remote-user']);
  $prompt = "Are there any special ssh options needed to connect to this server? eg an alternate port or authentication method? \nIf so, enter the additional ssh options here now. eg [-p 100]";
  $settings['ssh-options'] = call_user_func(DRUSH_READLINE_PREFILLED, $prompt, $settings['ssh-options']);

  // Save things temporarily before we continue
  clone_save_alias($settings);
  
  drush_print("{$ACTION}Testing connection to {$settings['remote-host']}.{$_ACTION}", INFO_INDENT);
  drush_print("If you have not yet set up ssh authentication, you may be prompted for a password.", INFO_INDENT);

  // Now see if we can connect
  $remote_command = 'pwd';
  $command = sprintf("ssh %s %s@%s %s", $settings['ssh-options'], escapeshellarg($settings['remote-user']), escapeshellarg($settings['remote-host']), escapeshellarg($remote_command));
  // Use sombnall of drush backend. Drush uses JSON, but I'm just looking for shell feedback
  // as we don't have drush on the remote site.
  $proc = _drush_proc_open($command);
  #  print_r($proc);
  if ($proc['code'] == 0 && $proc['output']) {
    drush_print("Remote login dir is ". trim($proc['output']), INFO_INDENT);
  }
  else {
    drush_set_error($proc['code'], dt("There was a problem running the remote command. \n". $command));
    return FALSE;
  }
  
  drush_log("{$HIGHLIGHT}Initial connection seems to have succeeded{$_HIGHLIGHT}", 'ok');

  // Option to configure public key if not already done
  clone_setup_ssh($settings);

  clone_setup_drush($settings);
  // Drush setup may have set the drush path - save it.
  clone_save_alias($settings);

  return TRUE;
}

function clone_save_alias($alias_record, $alias_file_path = NULL) {
  if (empty($alias_file_path)) {
    $alias_file_path = clone_alias_file_path($alias_record);
  }
  drush_log(dt("Saving alias settings as !alias_file_path", array('!alias_file_path' => $alias_file_path)), 'info');
  // _drush_sitealias_print_record() isn't fit for re-use.
  // Do it ourselves
  $exported_alias = var_export($alias_record, TRUE);
  $alias_config = "<?php\n// Alias config file generated by ".basename(__FILE__). "\n\n" . '$aliases[\'' . $alias_record['name'] . '\'] = ' . $exported_alias . ';';
  file_put_contents($alias_file_path, $alias_config);
}

/**
 * @param if global is set (default) then the alias files will be in the current
 * user directory.
 * If not, then in the site folder for the named site.
 */
function clone_alias_file_path($settings, $global = TRUE) {
  $save_file_name = $settings['name'].'.alias.drushrc.php';
  if ($global) {
    $settings_file_path = drush_server_home() . '/.drush/' . $save_file_name;
  }
  else {
    // Ensure the site_path is available
    _drush_sitealias_add_transient_defaults($settings);
    $site_path = $settings['path-aliases']['%site'];
    $settings_file_path = "{$settings['root']}/{$site_path}{$save_file_name}";
  }
  return $settings_file_path;
}

/**
 * Just used as example pre-fillers to assist making a site-alias setting from
 * scratch.
 */
function clone_default_settings() {

#$options['site-aliases']['stage'] = array(
#    'uri' => 'stage.mydrupalsite.com', // Note, not a URI!
#    'root' => '/path/to/remote/drupal/root',
#    'db-url' => 'pgsql://username:password@dbhost.com:port/databasename',
#    'remote-host' => 'mystagingserver.myisp.com',
#    'remote-user' => 'publisher',
#    'path-aliases' => array(
#      '%drush' => '/path/to/drush',
#      '%drush-script' => '/path/to/drush/drush',
#      '%dump' => '/path/to/live/sql_dump.sql',
#      '%files' => 'sites/mydrupalsite.com/files',
#      '%custom' => '/my/custom/path',
#     ),
#  );

  $default_settings = array(
    'name' => drush_get_option(array('name'), 'mydrupalsite.remotehost'),
    'remote-host' => drush_get_option(array('host'), trim(`hostname`)),
    'remote-user' => drush_get_option(array('user'), trim(`whoami`)),
    'ssh-options' => drush_get_option(array('ssh-options'), ''),
    'root' => drush_get_option(array('root'), '/var/www'),
    'uri' => drush_get_option(array('uri'), 'default'),
    'db-url' => drush_get_option(array('db-url'), 'mysqli://username:password@dbhost.com:port/databasename'),
  );
  return $default_settings;
}  

/**
 * UI detour to list and choose a site-alias by name from those available
 */
function drush_clone_choose_alias($message = "") {
  if (clone_confirm_prefilled("Do you want to see a list of known site-aliases (drush site-alias) ?", 'n')) {
    $all_aliases = _drush_sitealias_all_list();
    print(join("\n", array_keys($all_aliases)));
  }
  $alias = clone_readline_prefilled("Enter a site alias $message > ", '');
  return $alias;
}


/**
 * Option to configure a remote public key on the target.
 * 
 * $settings should be a remote site alias configuration array,
 * defining an already-tested remote connection.
 */
function clone_setup_ssh($settings) {
  extract($GLOBALS['text_decorations']);
  drush_log(dt("{$ACTION}Confirming ssh login is set up{$_ACTION}"), 'info');

  // Guide you through ssh key setup
  $pub_key_path = getenv('HOME').'/.ssh/id_rsa.pub';
  if (! file_exists($pub_key_path)) {
    drush_set_error('no public key', dt("You have no local public authentication key. Could not find $pub_key_path"));
    if (drush_confirm("Set up ssh public-key identity locally?") ) {
      $success = passthru('ssh-keygen');
    }
  }
  drush_print("Would you like to insert your public key on the remote server for easier connection from now on?", INFO_INDENT);
  drush_print("This process will allow you to log in to this target host automatically, by placing a copy of your local public key into the [authorized_keys] file over there.", INFO_INDENT);

  #$confirm = clone_readline_prefilled("Set up public-key authentication? (Skip this if it's already done!)", "n");
  #$confirmed = ($confirm == 'y');
  #if ($confirmed) {

  #if (drush_confirm("Set up public-key authentication? (Skip this if it's already done!)") ) {
  if (clone_confirm_prefilled("Set up public-key authentication? (Skip this if it's already done!)", 'n')) {
    drush_print("{$ACTION}Pushing your ssh key to {$settings['remote-host']}.{$_ACTION}");
    $public_key = file_get_contents($pub_key_path);
    // the mkdir may fail, but so what.
    $command = "mkdir ~/.ssh; echo '$public_key' >> .ssh/authorized_keys ;";
    $proc = clone_run_remote_proc($command, $settings);
    if ($proc['code'] != 0) {
      drush_set_error($proc['code'], dt("Failed to insert your key on the remote server"));
      return FALSE;
    }
    drush_log("{$HIGHLIGHT}Key insertion seems to have succeeded.{$_HIGHLIGHT} Life is good.", 'ok');    
  }

  return TRUE;
}


/**
 * Option to remotely install drush!
 * 
 * $site_record should be a remote site alias configuration array, defining an
 * already-tested remote connection.
 * 
 * $site_record['path-aliases']['%drush-script'] may be updated with an
 * appropriate value if successful
 */
function clone_setup_drush(&$site_record) {
  extract($GLOBALS['text_decorations']);
  drush_log(dt("{$ACTION}Confirming drush is available on remote site{$_ACTION}"), 'info');

  // Loop until success.
  while (! $success = clone_check_remote_drush($site_record)) {
    drush_print('Running drush remotely failed. Do you know the path of drush on the remote site?');
    if ($drush_path = clone_readline_prefilled("Enter the path to the drush executable on the target site. Blank if unknown, or you want to install it automatically).", @$site_record['path-aliases']['%drush-script'])) {
      $site_record['path-aliases']['%drush-script'] = $drush_path;
    }
    else {
      break;
    }
  }
  if ($success) {
    drush_log('Running drush remotely seems to have worked!', 'ok');
    return TRUE;
  }

  drush_print("Would you like to install drush over there? I will be useful in the long run.", INFO_INDENT);
  if (clone_confirm_prefilled("Install drush on the target? (Skip this if it's already done!)", 'n')) {
    drush_print("{$ACTION}NOT IMPLIMENTED{$_ACTION}");
    // the mkdir may fail, but so what.
    #$command = "mkdir ~/.ssh; echo '$public_key' >> .ssh/authorized_keys ;";
    #$proc = clone_run_remote_proc($command, $settings);
    #if ($proc['code'] != 0) {
    #  drush_set_error($proc['code'], dt("Failed to insert your key on the remote server"));
    #  return FALSE;
    #}
    #drush_print("{$HIGHLIGHT}Key insertion seems to have succeeded.{$_HIGHLIGHT} Life is good.");    
  }
  return TRUE;
}

/**
 * Input and verify the location and name of the remote Drupal instance
 * 
 * This will return immediately on partial failures. The calling function should
 * wrap it in a retry loop.
 * 
 * It will save partial successes as it goes.
 */
function clone_setup_remote_drupal_settings(&$settings) {
  extract($GLOBALS['text_decorations']);
  
  $prompt = "What is the path to *drupal* root on the remote machine? Enter a path relative to your login_dir or absolute on that machine.";
  $settings['root'] = call_user_func(DRUSH_READLINE_PREFILLED, $prompt, $settings['root']);

  $prompt = dt("Now enter the instance *foldername* that this site is stored in in the remote site. Enter 'default' here if not using multisite. enter the site name [@name] if using multisite. Do not include 'sites'. (this is the alias 'uri' setting)", array('@name' => $settings['name']));
  $settings['uri'] = call_user_func(DRUSH_READLINE_PREFILLED, $prompt, $settings['uri']);

  // Save things temporarily, to avoid re-entry
  clone_save_alias($settings);

  // Now log in remotely again and check that these settings check out.
  drush_print("{$ACTION}Checking Drupal root path is correct{$_ACTION}", INFO_INDENT);
  if (! clone_check_remote_connection($settings)) {
    return FALSE;
  }

  drush_log("{$HIGHLIGHT}Drupal core seems to be available at {$settings['root']}.{$_HIGHLIGHT} How nice.", 'ok');    
  
  drush_print("{$ACTION}Checking Drupal site instance path is correct{$_ACTION}", INFO_INDENT);
  $settings['path-aliases']['%site'] = $site = 'sites/'. $settings['uri'];
  $command = "ls {$settings['root']}/{$site}; ls {$settings['root']}/{$site}/settings.php; ";
  $proc = clone_run_remote_proc($command, $settings);
  if ($proc['code'] != 0) {
    drush_set_error($proc['code'], dt("Failed to find a Drupal site instance in the expected place ({$settings['root']}/{$site}) on the remote server. No settings.php file was found in that directory."));
    return FALSE;
  }
  drush_log("{$HIGHLIGHT}Some Drupal site settings seem to exist at {$settings['root']}/{$site}.{$_HIGHLIGHT} Groovy.", 'ok');    

  if (! isset($settings['path-aliases']['%dump'])) {
    $settings['path-aliases']['%dump'] = '/tmp/drupal-dump.sql';
  }  
  $prompt = dt("To help syncronization, we should have a temporary dump file path where database scripts can be created. It's best if this is outside of web root. Enter a path here.", array());
  $settings['path-aliases']['%dump'] = call_user_func(DRUSH_READLINE_PREFILLED, $prompt, $settings['path-aliases']['%dump']);

  // With this information, we can probably fetch the database settings.
  // About now, drush alias can probably start to be used to fetch that for us.
  // Use the user@server/path/to/drupal#sitename syntax
  // ... but no, not if drush is not already on the remote, we can't call backend-invoke.
  # $alias = $settings['remote-user'] .'@'. $settings['remote-host'] .'/'. $settings['root'] .'#'. $settings['uri'];
  # $remote_settings = drush_sitealias_get_record($alias, TRUE);
  #$remote_alias_settings = drush_backend_invoke("@$settings['uri'] site-alias --with-db {$settings['uri']}");
  #print_r( $remote_alias_settings );
  #print_r(  sitealias_get_databases_from_record($settings) );

  clone_save_alias($settings);
  return TRUE;
}

/**
 * Return TRUE or FALSE if the alies settings are sufficient to log in.
 *  and find
 * Drupal
 */
function clone_check_remote_connection($settings) {
  $command = "ls {$settings['root']}; ";
  $proc = clone_run_remote_proc($command, $settings);
  if ($proc['code'] != 0) {
    drush_set_error($proc['code'], dt("Failure result from remote server"));
    drush_log(dt("Failed running remote command on remote server %remote-user@%remote-host", array('%remote-host' => $settings['remote-host'], '%remote-user' => $settings['remote-user'])), 'warning');
    return FALSE;
  }
  return TRUE;
}

/**
 * Return TRUE or FALSE if the alies settings are sufficient to log in and find
 * Drupal
 */
function clone_check_remote_drupal_core($settings) {
  $command = "ls {$settings['root']}/index.php; ";
  $proc = clone_run_remote_proc($command, $settings);
  if ($proc['code'] != 0) {
    drush_log(dt("Failed to find a Drupal core install at @root on the remote server", array('@root' => $settings['root'])), 'warning');
    return FALSE;
  }
  return TRUE;
}


/**
 * Return TRUE or FALSE if it seems there is a working copy of drush over
 * there.
 * 
 * The proper invocation for remote drush calls
 * @see drush_do_site_command()
 * .. but we need to try even lower-level, just to see if the drush path exists.
 */
function clone_check_remote_drush($site_record) {

  if (! isset($site_record['path-aliases']['%drush-script']) ) {
    drush_log(dt('There is no specific %drush-script path set in this site alias record. If drush is installed on the remote site, but not available in the PATH, it may help to set the full path to the %site-alias explicitly in the settings. Trying to run drush remotely anyway. '), 'warning');
  }
  $drush_path = isset($site_record['path-aliases']['%drush-script']) ? $site_record['path-aliases']['%drush-script'] : 'drush' ;

  $command = "$drush_path -v; ";
  $proc = clone_run_remote_proc($command, $site_record);
  if ($proc['code'] != 0) {
    drush_log(dt("Failed to run drush on @remote-user@@remote-host", array('@remote-host' => $site_record['remote-host'], '@remote-user' => $site_record['remote-user'])), 'warning');
    return FALSE;
  }
  return TRUE;
}





/**
 * Run a given command on a remote server, defined by the connection settings
 * 
 * Returns the drush 'proc' style of result
 */
function clone_run_remote_proc($remote_command, $settings) {
  drush_log(dt("Running remote command : !command on !host", array('!command' => $remote_command, '!host' => $settings['remote-host'])), 'debug');

  $command = sprintf("ssh %s %s@%s %s", $settings['ssh-options'], escapeshellarg($settings['remote-user']), escapeshellarg($settings['remote-host']), escapeshellarg($remote_command));
  drush_log(dt("Running : $command"), 'debug');
  // Use sombnall of drush backend. Drush uses JSON, but I'm just looking for shell feedback
  // as we don't have drush on the remote site.
  return _drush_proc_open($command);
}


##########################################
/*
 * I'd like to prefill a value, but that seems hard without readline support
 */

function clone_readline_prefilled($prompt = '> ', $prefill = '') {
  print "\n" . $prompt;
  if ($prefill) print "\n[$prefill]> ";
  else { print "\n> ";}
  $line = trim(fgets(STDIN));
  if (empty($line)) $line = $prefill;
  return $line;
}

/**
 * Taken from drush_confirm.
 */
function clone_confirm_prefilled($prompt = '(y/n):', $default = 'n') {
  drush_print("\n". $prompt . " (y/n): ");
  if ($default) print "[$default]> ";

  // Automatically accept confirmations if the --yes argument was supplied.
  if (drush_get_context('DRUSH_AFFIRMATIVE')) {
    print "y\n";
    return TRUE;
  }
  // Automatically cancel confirmations if the --no argument was supplied.
  elseif (drush_get_context('DRUSH_NEGATIVE')) {
    print "n\n";
    return FALSE;
  }
  // See http://drupal.org/node/499758 before changing this.
  $stdin = fopen("php://stdin","r");

  while ($line = fgets($stdin)) {
  $line = trim($line);
    if ($line == '') {
      $line = $default;
    }
    if ($line == 'y') {
      return TRUE;
    }
    if ($line == 'n') {
      return FALSE;
    }
    print $prompt . " (y/n): ";
  }
}

/**
 * Reads a line from STDIN with fgets.
 * 
 * This function is an alternative when readline extension is not
 * available. It does not support neither autocompletion or history.
 */
function _drush_clone_fgets($prompt = "drush> ") {
  do {
    print $prompt;
    $line = trim(fgets(STDIN));
  } while ($line == '');

  return $line;
}
