<?php

/**
 * Attempt to ensure that local modules are 'pristine' - that is completely in
 * sync with the tagged checkout.
 * 
 * Do so by looking at the list of modules that need updating, and their alleged
 * version.
 * Download the matching version into a shadow directory.
 * Check if there is any diff between them
 * 
 * CANNOT do this with rdiff, as cvs rdiff will not allow us to ignore the $Id
 * stamp which may have changed if we are using a competing version control.
 * 
 * This supports situations where a development shop has taken stable releases
 * and checked them in to their own source control system. We now want to know
 * what (if anything) they have changed, and whether we can upgrade the modules.
 * 
 * If there are diffs, extract them as 'local changes' patches.
 * 
 * THEN we can download updates from d.o.
 * 
 * The recorded diffs may sometimes be re-applied after upgrading the modules
 * with official versions, although conflicts may exist to be patched by hand.
 * 
 */

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * @return   An associative array describing your command(s).
 */
function merger_drush_command() {
  extract($GLOBALS['text_decorations']);
  $items = array();

  $items['check-clean'] = array(
    'description' => "Compare the current module with a fresh version checked out from d.o",
    'callback' => 'merger_drush_check_clean',
    'options' => array(
     ),
    'arguments' => array(
      'modules' => 'A space delimited list of modules.',
    ),
    'examples' => array(
      'check-clean webform' => 'See if there are any local changes against the currently-used webform code.',
    ),
    #'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

/**
 * This will involve downloading copies of the current checkout to a nearby
 * location before diffing them.
 */
function merger_drush_check_clean($project = array()) {
  if (function_exists('module_load_include')) {
    module_load_include('inc', 'system', 'system.admin');
  }
  else {
    // Drupal5 only.
    require_once('./'. drupal_get_path('module', 'system') .'/system.module');
  }
  
  $modules = func_get_args(); 
  drush_include_engine('drupal', 'environment');
  $module_info = drush_get_modules();
  $pristine_dir = getcwd().'/sites/all/modules-versioned';
  if (! file_exists($pristine_dir)) {
    mkdir($pristine_dir);
  }
  if (! is_writable($pristine_dir)) {
    drush_set_error(__FUNCTION__, dt("Unable to write to cache download directory !pristine_dir", array('!pristine_dir' => $pristine_dir)));
    return FALSE;
  }
  $unmodified = array();
  $modified = array();
  $unknown = array();
  $patchfiles = array();
  $modules = array();
  
  if (empty($modules)) {
    drush_print(dt('No module specified, checking ALL modules', array()));
    // Check ALL ENABLED modules
    foreach ($module_info as $module_name => $module) {
      if ($module_info[$module_name]->status) {
        if ( $proj_name = $module_info[$module_name]->info['project'] ) {
          // index on proj_name, not module name
          $enabled_projects[$proj_name] = $module_name;
        }
        else {
          $nicename = sprintf("[ %-20s ]", $module_name); // Just output formatting
          drush_print(dt('!module_name IS UNKNOWN or CVS-only, not comparing with any official release.', array('!module_name' => $nicename)));
          $unknown[$module_name] = $module_name;
        }
      }
    }
    $modules = $enabled_projects;
  }
  else {
    // Collect the info about this modules project.
    foreach ($modules as $module_name) {
      #print_r($module_info[$module_name]);
      if ( $project_name = $module_info[$module_name]->info['project'] ) {
        // index on proj_name, not module name
        $projects[$project_name] = $module_name;
      }
    }
    $modules = $projects;
  }
  if (empty($modules)) {
    drush_set_error(__FUNCTION__, dt("No valid modules specified, please enter the name of a currently enabled module."));
  }

  drush_print(dt('Checking freshness of : !modules', array('!modules' => implode(', ', $modules))));
  
  foreach ($modules as $project_name => $module_name) {
    // Note that most updates must be done by PROJECT name, not module name.
    // This could get confusing. Don't update 'cck', update 'content'
    $info = $module_info[$module_name]->info;
    $current_version = $info['version'];
    $nicename = sprintf("[ %-20s ]", $project_name); // Just output formatting
    
    if (empty($current_version)) {
      // This should not happen. Occurred when the phoject/module name was out of sync 
      drush_log(dt("!project_name IS UNKNOWN STATUS. It seems to have no release information. Cannot compare with the stable version.", array('!project_name' => $nicename)), 'warning');
      $unknown[$project_name] = $project_name;
      dlm($module_info[$project_name]);
      print_r($module_info[$project_name]);
      continue;
    }
    #print_r($module_info[$module_name]);

    // Is there an API to get the TAG? why isn't that part of the info?
    $current_tag = "DRUPAL-". preg_replace('/\.x?/', '-', $current_version);
    drush_log(dt('Local project !module_name is apparently [!project_name !module_version !tag]. Comparing it against a clean checkout.', 
      array(
        '!module_name' => $info['name'], 
        '!project_name' => $project_name, 
        '!module_version' => $info['version'], 
        '!tag' => $current_tag,
        )
      ));
    
    // Download exactly that into a nearby named directory
    $save_as = "{$pristine_dir}/$project_name-$current_version";
    if (! is_dir($save_as)) {
      // Avoid doing this each time
      $command = "dl -q --destination=$save_as $project_name-$current_version"; 
      drush_print(dt('Fetching a fresh copy of !module_version ', array('!module_version' => "$project_name-$current_version" )));
      drush_log(dt("Downloading !project_name to !save_as \n  !cmd", array('!project_name' => $info['name'], '!save_as' => $save_as, '!cmd' =>  $command))); 
      drush_backend_invoke($command);
    }
    // The drush dl will have nested the download result slightly, OK, whatever, I can live with that.
    $pristine_module_path = "$save_as/$project_name";

    if ($project_name == 'drupal') {
      // The core download names this with the version attached already. 
      // This double-nests it. Workaraound is messy, so just deal.
      $pristine_module_path = "$save_as/$project_name-$current_version";
    }

    if (! is_dir($pristine_module_path)) {
      $debug_command = " drush -v -d $command";
      drush_set_error(__FUNCTION__, dt("Download of pristine project '!project_name' !current_version to '!pristine_module_path' apparently failed. May be network, permissions, or a corrupt/unavailable package. To diagnose, you may try running the download yourself:\n !debug_command", array('!project_name' => $project_name, '!current_version' => $current_version, '!pristine_module_path' => $pristine_module_path, '!debug_command' => $debug_command)));
      continue;
    }
#print_r($module_info[$module_name]);
    
    // Now do the diff.
    // The whole reason we had to do this the long way was do we could ignore some lines
    // and files.
    $current_project_path = merger_find_project_path($module_name, $project_name);
    
    $diff_args = " -u"; // show unified context
    $diff_args .= ' --recursive';
    $diff_args .= ' --ignore-matching-lines="\\$Id"'; // Here's the thing that local CVS may have messed with
    $diff_args .= ' --ignore-matching-lines="\\$Date"'; 
    $diff_args .= ' --ignore-matching-lines="\\$Name"'; // These are rare, but CVS modifies them
    $diff_args .= ' --exclude=.project'; // Eclipse. Do not ignore '.' entirely, because we do care about .htaccess.
    $diff_args .= ' --exclude=.svn'; // 
    $diff_args .= ' --exclude=_notes'; // O_o - some leftover trash from Dreamweaver.
    $diff_args .= ' --exclude="*.patch"'; // patches on patches would get recursive
    $diff_args .= ' --exclude="*.info"'; // Although this may be important, the packaging script introduces irrelevant changes
    $diff_args .= ' --exclude=LICENSE.txt'; // packaging script added this, it's not in CVS 
    $diff_args .= ' --exclude=backup'; // this is created (in drupal root) by drush update itself.
    $diff_args .= ' --ignore-space-change '; // maybe from debugging
    $diff_args .= ' --show-c-function '; // handy
    $diff_args .= ' --new-file'; // make a note of the content of newly added files even

    $no_source_control = ' --exclude=CVS';
    $no_source_control .= ' --exclude=.svn';

    if ($project_name == 'drupal') {
      $current_project_path = "./"; 
      $diff_args .= ' --exclude=sites';
    }
    // Patches that are made contain full site-relative paths
    // This is a bit cumbersome, but correct enough, as we will
    // Only use them here.

    $exec = "diff $diff_args $no_source_control $pristine_module_path $current_project_path "; 
    drush_log(dt("... Evaluating significant differences between current code and original release code.\n!cmd", array('!cmd' => $exec) )); 

    // Run but don't save the diff results, just need to know there are any
    $output = ""; $return_var = NULL;
    exec($exec, $output, $return_var);

    drush_log("Evaluated diff", 'debug');
    if (empty($output)) {
      drush_print("$nicename IS SAFE TO UPDATE! There is no significant difference found in the code.");
      $unmodified[$project_name] = $project_name;
    }
    else {
      $diff_file = dirname($current_project_path) . "/$project_name-$current_version-to_local-". date('Ymd') .".patch";
      drush_log(dt("!module_name HAS BEEN MODIFIED LOCALLY! Saving diffs into !diff_file.", array('!module_name' => $nicename, '!diff_file' => $diff_file)), 'warning');
      $modified[$project_name] = $project_name;
      $exec .= " > $diff_file"; 
      exec($exec, $output, $return_var);
      $patchfiles[$project_name] = $diff_file;

      // Diagnostic help: Show the dirty diff only if -d debug is on
      $diff_results = file_get_contents($diff_file);
      drush_log("----------\n Diff between $project_name-$current_version and $current_project_path \n-------------------\n$diff_results\n------------", 'notice'); 
    }
    
    // EVEN if it's not modified... take a diff of CVS directory changes so we can restore this after an upgrade.
    // The exec here skips the $no_source_control  option - so it now will include CVS dirs
    $diff_file = dirname($current_project_path) . "/$project_name-$current_version-source_control-". date('Ymd') .".patch";
    $exec = "diff $diff_args $pristine_module_path $current_project_path "; 
    drush_log(dt("Making a note of trivial differences anyway (source control files). We may need to remake CVS dirs after an update.", array()), 'debug');
    $exec .= " > $diff_file"; 
    exec($exec, $output, $return_var);
    $source_control_patchfiles[$project_name] = $diff_file;

    // Diagnostic help:
    $diff_results = file_get_contents($diff_file);
    drush_log("----------\n Source-control-only Diff between $project_name-$current_version and $current_project_path \n---------$diff_file----------\n$diff_results\n------------", 'debug'); 
  }

  // Allow an immediate update of known-safe modules
  if (count($unmodified)) {
    $msg = "Do you want to check for available updates and update all the KNOWN SAFE modules we have identified?";
    if (drush_confirm($msg)) {
      drush_print("Running pm-update to fetch release information from the drupal.org. Unmodified projects to update:". join(' ', $unmodified));

      $command = "pm-update ".join(' ', $unmodified);
      drush_backend_invoke($command);
      
      // Problem. running update will move the old dirs away and replace them
      // This means that the old non-drupal CVS info goes away too!
      // Which puts our repository weird again.
      //
      // Re-apply the remembered $source_control_patchfiles[$project_name]
      // This will put (almost) everything back the way it was.
      foreach ($unmodified as $project_name) {
        $patchfile = $source_control_patchfiles[$project_name]; 
        $exec = "patch -p0 --input=$patchfile";
        exec($exec, $output, $return_var);
        drush_log(dt("Replaced the source control information into the updated !project_name project folder. You can now use it to see the changes that were made.", array('!project_name' => $project_name)), 'notice');
      }
    }  
  }
  if (count($modified)) {
    $msg = dt("Do you want to attempt to upgrade the !count MODIFIED modules? We will apply the changes back to the downloaded files if we can, but this may break things. Backups will be taken, and a log of the differences is already made.", array('!count' => count($modified)));
    if (drush_confirm($msg)) {
      // Upgrade them, this will remove our local changes for a minute
      drush_print("Running pm-update to fetch release information from the drupal.org. Modified projects to update: ". join(' ', $modified));

      // I don't actually know which ones NEED an upgrade though.
      // And calling pm-update individually is way too slow.
      $command = "pm-update ".join(' ', $modified);
      drush_backend_invoke($command);

      drush_confirm("Done updates, code should be fresh. About to re-apply the local changes");
      // Now re-apply our changes
      foreach ($modified as $project_name) {
        $patchfile = $source_control_patchfiles[$project_name]; 
        $exec = "patch -p0 --input=$patchfile";
        exec($exec, $output, $return_var);
        drush_log(dt("Replaced the source control information into the updated !project_name project folder. You can now use it to see the changes that were made.", array('!project_name' => $project_name)), 'notice');
        if ($return_var) { // any result is an error, FALSE is ok
          drush_log(dt("Result of applying the patch\n-----------\n!exec\n----------\n!output\n--------------------.", array('!exec' => $exec, '!output' => join("\n", $output))), 'warning');
        }
      }
      drush_log("Finished update, and re-applied any local changes we needed to retain.\nYou should test the results right away!", 'warning');
    }  
  }
  
  // These are trivial and messy. Throw them away when done
  // The source-control patchfiles are temporary, but the local changes patchfiles
  // are left behind for inspection (although unused)
  drush_log(dt("Cleaning up the trivial patch files.", array()));
  foreach($source_control_patchfiles as $patchfile) {
    unlink($patchfile);
  }

}

/**
 * Return the directory of the PROJECT containing this module.
 * 
 * Need to do some magic if this is a sub-module, eg cck/modules/content_copy
 * Loop up the parents till we find a dir named after the Project
 */
function merger_find_project_path($module_name, $project_name) {
  $current_module_path = drupal_get_path('module', $module_name);
  if ($project_name != $module_name) {
    // // I dunno, chop dirs off until we hit the project dir.
    $dirs = explode('/', $current_module_path);
    while ($dirs && array_pop($dirs) != $project_name) { /* loop */ }
    if (! empty($dirs) ) {
      $dirs[] = $project_name;
      $current_module_path = join('/', $dirs);
      drush_log("Climbed up to find the project dir that contains sub-module $module_name is actually $current_module_path ", 'debug');
    }
  }
  return $current_module_path;
}